/**
* ===================================================================
* main js
*
* -------------------------------------------------------------------
*/

(function($) {

  "use strict";

  /*---------------------------------------------------- */
  /* Preloader
  ------------------------------------------------------ */
  $(window).load(function() {

    $("#year").html(new Date().getFullYear())

    // will first fade out the loading animation
    $("#loader").fadeOut("slow", function(){

      // will fade out the whole DIV that covers the website.
      $("#preloader").delay(300).fadeOut("slow");

    });

  })

  $('.focus-subscribe').click(function() {
    setTimeout(function () {
      $('#mc-email').focus();
    }, 1000);
  })


  /*----------------------------------------------------*/
  /*	Sticky Navigation
  ------------------------------------------------------*/
  $(window).on('scroll', function() {

    var y = $(window).scrollTop(),
    topBar = $('header');

    if (y > 1) {
      topBar.addClass('sticky');
    }
    else {
      topBar.removeClass('sticky');
    }

  });


  /*-----------------------------------------------------*/
  /* Mobile Menu
  ------------------------------------------------------ */
  var toggleButton = $('.menu-toggle'),
  nav = $('.main-navigation');

  toggleButton.on('click', function(event){
    event.preventDefault();

    toggleButton.toggleClass('is-clicked');
    nav.slideToggle();
  });

  if (toggleButton.is(':visible')) nav.addClass('mobile');

  $(window).resize(function() {
    if (toggleButton.is(':visible')) nav.addClass('mobile');
    else nav.removeClass('mobile');
  });

  $('#main-nav-wrap li a').on("click", function() {

    if (nav.hasClass('mobile')) {
      toggleButton.toggleClass('is-clicked');
      nav.fadeOut();
    }
  });


  /*----------------------------------------------------*/
  /* Highlight the current section in the navigation bar
  ------------------------------------------------------*/
  var sections = $("section"),
  navigation_links = $("#main-nav-wrap li a");

  sections.waypoint( {

    handler: function(direction) {

      var active_section;

      active_section = $('section#' + this.element.id);

      if (direction === "up") active_section = active_section.prev();

      var active_link = $('#main-nav-wrap a[href="#' + active_section.attr("id") + '"]');

      navigation_links.parent().removeClass("current");
      active_link.parent().addClass("current");

    },

    offset: '25%'

  });


  /*----------------------------------------------------*/
  /* Flexslider
  /*----------------------------------------------------*/
  $(window).load(function() {

    $('#testimonial-slider').flexslider({
      namespace: "flex-",
      controlsContainer: "",
      animation: 'slide',
      controlNav: true,
      directionNav: true,
      smoothHeight: true,
      slideshowSpeed: 7000,
      animationSpeed: 600,
      randomize: false,
      touch: true,
    });

  });


  /*----------------------------------------------------*/
  /* Smooth Scrolling
  ------------------------------------------------------*/
  $('.smoothscroll').on('click', function (e) {

    e.preventDefault();

    var target = this.hash,
    $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 800, 'swing', function () {
      window.location.hash = target;
    });

  });


  /*----------------------------------------------------*/
  /*  Placeholder Plugin Settings
  ------------------------------------------------------*/

  $('input, textarea, select').placeholder()


  /*---------------------------------------------------- */
  /* Newsletter mail sender
  ------------------------------------------------------ */

  $('#mc-form').submit((event) => {
    $(".subscribe-message").html('Enviando...')
    $.ajax({
        url: "/mail/" + $('#mc-email').val()
      , success: (result) => {
        $(".subscribe-message").html('<i class="icon-yes"></i> Obrigado pelo contato, retornaremos com novidades em breve :)')
      }
      , error: (XMLHttpRequest, textStatus, errorThrown) => {
          $(".subscribe-message").html('<i class="icon-warning"></i> Ops, algo de errado aconteceu...Verifique o email digitado e tente novamente.')
      }
    })
    event.preventDefault()
  })


  /*----------------------------------------------------- */
  /* Back to top
  ------------------------------------------------------- */
  var pxShow = 300; // height on which the button will show
  var fadeInTime = 400; // how slow/fast you want the button to show
  var fadeOutTime = 400; // how slow/fast you want the button to hide
  var scrollSpeed = 300; // how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'

  // Show or hide the sticky footer button
  jQuery(window).scroll(function() {

    if (!( $("#header-search").hasClass('is-visible'))) {

      if (jQuery(window).scrollTop() >= pxShow) {
        jQuery("#go-top").fadeIn(fadeInTime);
      } else {
        jQuery("#go-top").fadeOut(fadeOutTime);
      }

    }

  });

})(jQuery);
