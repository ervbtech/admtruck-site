'use strict'
const mailer  = require("nodemailer");
const options =  {
    transporter: 'smtps://noreply%40admtruck.com.br:senha%40123@smtp.zoho.com'
  , to: 'contact@admtruck.com.br'
}
module.exports.send = (mailFrom, subject, message, isHtml) => {
  return new Promise((resolve, reject) => {
    let _mail = {
      from: mailFrom
    , to: options.to
    , subject: subject
    };

    if(isHtml) {
      _mail.html = message
    } else {
      _mail.text = message
    }

    mailer.createTransport(options.transporter).sendMail(_mail, (err) => {
      if(err) {
        reject(err)
      } else {
        resolve()
      }
    })

  })
}
