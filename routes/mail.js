'use strict'


const express = require('express')
const router  = express.Router()
const sender  = require('../utils/sender')
const fs      = require('fs')


router.get('/mail/:mailAddress', (req, res, next) => {
  fs.readFile(require('path').resolve(__dirname, '../resources/mails/subscribe-newsletter.html'), 'utf8', (err, data) => {
    if(!err) {
        let mailAddress = req.params.mailAddress
        sender.send('noreply@admtruck.com.br', 'Inscrição Newsletter', data.replace('{0}', mailAddress), true)
        .then(data => {
          res.send('Email sended success')
        })
        .catch((err) => {
          res.status(500).send(err)
        })
    } else {
      res.status(404).send(err)
    }
  });
});

module.exports = router
